package com.example.todo.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "ToDo")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ToDo {
     private long userId;
    @Id
    private long id;
    private String title;
    private boolean completed;
}
