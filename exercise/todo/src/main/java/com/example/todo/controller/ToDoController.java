package com.example.todo.controller;

import com.example.todo.model.ToDo;
import com.example.todo.repository.ToDoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@RestController
@RequestMapping("/todo")
public class ToDoController {
    @Autowired
    ToDoRepository toDoRepository;
    @RequestMapping(value = "/add",method= RequestMethod.POST,produces = "application/json")
    public ResponseEntity addToDo(@RequestBody ToDo toDo) {
        toDoRepository.save(toDo);
        return new ResponseEntity<>(toDo,HttpStatus.OK);

    }

    @RequestMapping(value = "/getList",method= RequestMethod.GET,produces = "application/json")
    public List<ToDo> getListToDo() {
            List<ToDo> toDoList=new ArrayList<ToDo>();
            toDoRepository.findAll().forEach(toDoList::add);
            return new ResponseEntity<>(toDoList, HttpStatus.OK).getBody();
    }




}
