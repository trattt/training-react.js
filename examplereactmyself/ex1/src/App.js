import logo from './logo.svg';
import './App.css';
import {Component} from "react";
import Age from "./components/Age"
import {BrowserRouter as Router,Route,Routes,Link} from "react-router-dom";
import Name from "./components/Name";
import Address from "./components/Address";

class App extends Component{
  render() {
    return (
    <Router>
      <div className={"App"}>
          <ul>
            <li>
              <Link to='/'>Home</Link>
            </li>
            <li>
              <Link to='/name'>Name</Link>
            </li>
            <li>
              <Link to='/age'>Age</Link>
            </li>
            <li>
              <Link to='/address'>Address</Link>
            </li>
          </ul>
      </div>
          <Routes>
            <Route extract path='/name' element={<Name />}></Route>
            <Route extract path='/age' element={<Age />}></Route>
            <Route extract path='/address' element={<Address />}></Route>
          </Routes>
    </Router>
  );
  }
}

export default App;
