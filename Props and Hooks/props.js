Ta dùng prop để gửi dữ liệu đến component
Mọi component là 1 hàm js thuần khiết (Pure function)
Trong React, prop tương đương với các tham số của các hàm js thuần khiết
Prop là bất biến. Trong các hàm js thuần khiết, ta không thể thay đổi được các dữ liệu của tham số

Có thể khai báo prop là 1 kiểu js cụ thể. Mặc định là optional
    optionalArray: PropTypes.array,
	
Bạn cũng có thể khai báo prop là 1 thể hiện của class
Cái này sử dụng toán tử instanceOf của js
    optionalMessage: PropTypes.instanceOf(Message),
	
Prop có thể bị giới hạn bởi giá trị cụ thể như enum	
    optionalEnum: PropTypes.oneOf(['News', 'Photos']),

Một object có thể là 1 hoặc nhiều loại
   optionalUnion: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.instanceOf(Message)
    ]),

 Một object có cảnh báo của các thuộc tính bổ sung (extra properties)
    name: PropTypes.string,
    optionalObjectWithStrictShape: PropTypes.exact({
      quantity: PropTypes.number
    }),

Bạn có thể xâu chuỗi bâst cứ th nào ở trên với 'isRequired' để đẩm bảo cảnh báo
được hiển thị nếu prop không được cung cấp
    requiredFunc: PropTypes.func.isRequired,
	
 // Giá trị bâ kì của bất kỳ kiểu dữ liệu
    requiredAny: PropTypes.any.isRequired,

// Bạn cũng có thể xác định xác trình xác thực tùy chỉnh. Nó nên trả về 1 lỗi
// object nếu xác thực fails. Đừng 'console.warn' hoặc throw vì nó sẽ không thực hiện
// trong 'oneOfType'
    customProp: function(props, propName, componentName) {
        if (!/matchme/.test(props[propName])) {
            return new Error(
                'Invalid prop `' + propName + '` supplied to' +
                ' `' + componentName + '`. Validation failed.'
            );
        }
    },

// Bạn có thể cung cấp trình xác thực tùy chỉnh cho 'arrayOf' và'objectOf'.
// Nó sẽ trả về 1 error object nếu xác thực fails. Xác thực sẽ call từng key trong
//     mảng hoặc object. Hai đối số đầu tiên của trình xác thực là chính mảng
//     hoặc object và khóa của item hiện tại.
    customArrayProp: PropTypes.arrayOf(function(propValue, key, componentName, location, propFullName) {
        if (!/matchme/.test(propValue[key])) {
            return new Error(
                'Invalid prop `' + propFullName + '` supplied to' +
                ' `' + componentName + '`. Validation failed.'
            );
        }
    })
};





