// import logo from './logo.svg';
import ComponentExample from "./components/ComponentExample";
// import ReactDOM from "react-dom/client";
function App(){
  // return (
  //   <div className="App">
  //     <header className="App-header">
  //       <img src={logo} className="App-logo" alt="logo" />
  //       <p>
  //         Edit <code>src/App.js</code> and save to reload.
  //       </p>
  //       <a
  //         className="App-link"
  //         href="https://reactjs.org"
  //         target="_blank"
  //         rel="noopener noreferrer"
  //       >
  //         Learn React
  //       </a>
  //     </header>
  //   </div>
  //


  //   npm install prop-types --save để thêm prop-types
  //   Cấu trúc
  //   ComponentClassName.propTypes{
  //
  //       propName1 : PropTypes.string,
  //           propName2 : PropTypes.bool,
  //           propName3 : PropTypes.array,
  //   .
  //   .
  //   .
  //   .
  //       propNamen : PropTypes.anyOtherType
  //   }
  //   Trong đó:
  //   ComponentClassName: tên lớp của Component
  //   anyOtherType: bất cứ type nào được truyền dưới dạng props
  //   Đối với các props không được validate loại dữ liệu được chỉ định từ
    // propsType, cảnh báo trên console sẽ xảy ra








return (
  <div className="App">
      <ComponentExample />
  </div>
);
}

export default App;
