import PropTypes from "prop-types";
import React from "react";
import ReactDOM from "react-dom";
class ComponentExample extends React.Component {
// <br/> xuống dòng
    render() {
        return (
            <div>
                {/* printing all props*/}
                <h1>
                    {this.props.arrayProp}
                    <br/>
                    {this.props.stringProp}
                    <br/>
                    {this.props.numberProp}
                    <br/>
                    {this.props.boolProp}
                    <br/>
                </h1>
            </div>
        );
    }
}

// Validate prop types
ComponentExample.propTypes={
    arrayProp: PropTypes.array,
    stringProp: PropTypes.string,
    numberProp: PropTypes.number,
    boolProp: PropTypes.bool,
}

// Tạo default props
ComponentExample.defaultProp={
    arrayProp: ['Ram', 'Shyam', 'Raghav'],
    stringProp: "GeeksforGeeks",
    numberProp: "10",
    boolProp: true,
}

// ReactDOM giữ vai trò cập nhật DOM để phù hợp với các React element
// DOM elenment trong js là tổng hợp những hàm như
// getElementById, getElementsByTagName, getElementsByClass và querySelectorAll
// Nhưng hàm này có công dụng là truy xuất đến các thẻ trong tài liệu
// DOM là tên gọi viết tắt của (Document Object Model – tạm dịch Mô hình Các Đối tượng Tài liệu)
// ReactDOM.render(
//     <ComponentExample />,
//     document.getElementById(`root`)
// );
export default ComponentExample
{
    ReactDOM.render(
        <ComponentExample/>,
        document.getElementById(`root`)
    );
}
