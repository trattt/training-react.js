import {Component} from "react";
// import ChildComponent vo ParentComponent
import ChildComponent from "./ChildComponent";
class ParentComponent extends Component {
    // render: hiển thị nội dung lên trình duyệt
    // Bước 1 tạo class ParentComponent
    // B2: Set 1 state cho parent (việc set a state không cần thiết nhưng nó làm cho
    // app dynamic). Tạo 1 event để đưa cảnh báo bất cứ khi nào component được render.
    // Đừng quên bind the event để từ khóa không trả về "undefined"

    // Nếu là component thì không được dùng các hook mà dùng constructor
    constructor(props) {
        super(props);
        this.state={ //state giống như 1 kho lưu trữ dữ liệu cho các component trong React
            // Nó chủ yếu dùng để cập nhật component khi người dùng thực hiện một số hành
            // động như nhấp vào nút, nhập một số văn bản, nhấn một số phím, v.v.
            parentName:'Parent'
        }
        this.greetParent=this.greetParent.bind(this)
    }

    greetParent(){
        alert('Hello ${this.state.parentName}') //hiển thị hội thoại cảnh báo khi muốn
        // đảm bảo thông tin đến người dùng
    }

    render() {
        return (
            <div>
                <ChildComponent greetHandler={this.greetParent()}/>
            </div>
        )
    }
}

export default ParentComponent;