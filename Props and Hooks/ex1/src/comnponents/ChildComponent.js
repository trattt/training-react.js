// B4: Tạo 1 component tên ChildComponent.js và tạo 1 function đơn giản
// Tạo một simple function và phương thức greetParent()
// Về cơ bản, button sẽ được nhấp vào khi geet the parent
function ChildComponent(props){
    return(
    //onClick dùng để xử lý event
        <div>
            <button onClick={()=>props.greetHandler()}>
                Greet Parent
                </button>
        </div>
    )
}
export default ChildComponent;