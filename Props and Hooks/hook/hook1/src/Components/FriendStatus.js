import {useEffect, useState} from "react";
import useFriendStatus from "./useFriendStatus";
function FriendStatus(props){
    // const [isOnline,setIsOnline]=useState(null);
    const isOnline=useFriendStatus(props.friend.id);
    // function handleStatusChange(status){
    //     setIsOnline(status.isOnline);
    // };

//     Trong ví dụ này React sẽ hủy đăng ký ChatAPI của chúng ta khi ngắt kết nối
//     component cũng như trước khi chạy lại effect cho render tiếp theo. (Nếu bạn muốn,
// có 1 cách yêu cầu React bỏ qua việc đăng ký lại props.friend.id mà chúng ta chuyển đến
// ChatAPI không thay đổi.)
//     Giống như useState, bạn có thể sử dụng nhiều hơn 1 effect trong 1 component
//     useEffect(()=>{
//         ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
//     });
    if (isOnline==null) {
        return 'Loading...';
    }
    return isOnline?'Online':'Offline'
}