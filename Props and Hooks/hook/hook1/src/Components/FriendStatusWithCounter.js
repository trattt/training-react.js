import {useState} from "react";

function FriendStatusWithCounter(){
    const [count,setCount]=useState(0);
    useState(()=>{
        document.title='You clicked ${count} times';
    })

    const [isOnline,setIsOnline]=useState(null);
    useState(()=> {
        ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);
        return () => {
            ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
        };
    });

    function handleStatusChanger(status){
        setIsOnline(status.isOnline);
    }
}


// Quy tắc Hooks
// - Chỉ Hooks ở cấp cao nhất. Không gọi Hooks bên trong các vòng lặp, điều kiện
// hoặc hàm lồng nhau
// - Chỉ call Hook từ các function component, không gọi Hooks qua các function
// thông thường (chỉ có 1 nơi hợp lệ khác gọi Hook ở nơi Hook bạn custom -custom hook)
