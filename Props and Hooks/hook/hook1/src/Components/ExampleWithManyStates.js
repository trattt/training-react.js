// Có thể khai báo nhiều trạng thái


import {useState} from "react";

function ExampleWithManyStates (){
    const [age,setAge]=useState(42);
    const [fruit,setFruit]=useState('banana');
    const [todos,setTodos]=useState([{text:'Learn Hooks'}]);


    Hooks là gì?
//         Hooks là các function cho phép bạn "hook into" với React state và
//     các tính năng vòng đời từ các hàm component. Hooks không làm việc bên
//     trong class - chúng cho phép bạn sử dụng React không cùng với class.
//     (Chúng ta sẽ không thể viết lại các component overnight hiện có nhưng bạn
// có thể bắt đầu sử dụng hooks trong một cái mới eếu muốn.)
//     React cung cấp một số hook tích hợp như useState. Bạn có thể tạo hooks của
//     riêng mình để sử dụng lại các các stateful behavior giữa các component khác
//     nhau. Chúng ta sẽ xem xét các hooks tích hợp trước.