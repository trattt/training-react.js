// Hooks mới được thêm vào từ React 16.8. Chúng cho phép bạn sử dụng state và
// các tính năng React khác mà không cần viết class

// Ví dụ này hiển thị một bộ đếm, khi click vào button sẽ tăng lên giá trị

import {useEffect, useState} from "react";

function Example(){
    // Khai báo biến new state, gọi là "count"
    // State giống như một kho lưu trữ dữ liệu cho các component trong ReactJS.
    // Nó chủ yếu được sử dụng để cập nhật component khi người dùng thực hiện
    // một số hành động như nhấp vào nút, nhập một số văn bản, nhấn một số
    // phím, v.v.
    const [count,setCount]=useState(0);

    // Tương tự với componentDidMount và componentDidUpdate:
    useEffect(()=>{
        //Update document title sử dụng browser API
        document.title='You clicked ${count} times';
    });



 // p: đoạn văn bản trong html
 //    useState không nhất thiết là đối tượng như this.state. IntialState là giá
    //    trị cho lần đầu tiên
    return (
        <div>
            <p>You click {count} times</p>
            <button onClick={()=>setCount(count+1)}>
                Click me
            </button>
        </div>
    )
}
