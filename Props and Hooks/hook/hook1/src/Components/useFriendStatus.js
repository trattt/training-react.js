import {useEffect, useState} from "react";
// function UseFriendStatus (friendID){
function useFriendStatus (friendID){
    // const [isOnline, setIsOnline] = useState(null);

    const isOnline=useFriendStatus(props.friend.id);

    function handleStatusChange(status){
        setIsOnline(status.isOnline);
    };

    useEffect(()=>{
        ChatAPI.subscribeToFriendStatus(friendID, handleStatusChange);
        return () => {
            ChatAPI.unsubscribeFromFriendStatus(friendID, handleStatusChange);
        };
    });
    return isOnline;
}

export default useFriendStatus();