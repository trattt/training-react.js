// import logo from './logo.svg';
import {Component} from "react";
import React from "react";
import {Fragment} from "react";
import ReactDOM from "react-dom";
import './App.css';
import
// import ClassCounterOne from "./components/ClassCounterOne ";
// import HookCounterOne from "./components/HookCounterOne";
// import {MemoryRouter as Router, Routes,Route,Link} from "react-router-dom";
// import {BrowserRouter as Router, Routes,Route,Link} from "react-router-dom";
import {HashRouter as Router, Routes,Route,Link} from "react-router-dom";
import Home from "./router/components/Home";
import About from "./router/components/About";
import Contact from "./router/components/Contact";

// class App extends Component
// {
//     // render() {
//     //     return (
//     //     <Router>
//     //         <div className={"App"}>
//     //             <ul>
//     //                 <li>
//     //                     <Link to= "/">Home</Link>
//     //                 </li>
//     //                 <li>
//     //                     <Link to= "/about">About us</Link>
//     //                 </li>
//     //                 <li>
//     //                     <Link to= "/contact">Contact us</Link>
//     //                 </li>
//     //             </ul>
//     //         </div>
//     //         <Routes>
//     //             <Route exact path='/' element={< Home />}></Route>
//     //             <Route exact path='/about' element={< About />}></Route>
//     //             <Route exact path='/contact' element={< Contact />}></Route>
//     //         </Routes>
//     //     </Router>
//     // );
//     // }
//
//     // render() {
//     //     return(
//     //         // element không liên quan
//     //         <Fragment>
//     //             <h2>Hello</h2>
//     //             <p>How you doin'?</p>
//     //         </Fragment>
//     //     );
//     // }
//     render() {
//         return(
//             // element không liên quan
//             <>
//                 <h2>Hello</h2>
//                 <p>How you doin'?</p>
//             </>
//         );
//     }
// }

// class App extends Component{
//     state={inputValue:''};
//
//     render() {
//         return (
//             <div>
//                 <form>
//                     <label>Enter text</label>
//                     <input type={"this.state.inputValue"}
//                     onChange={(e)=>this.setState({
//                         inputValue:e.target.value
//                     })}/>
//                 </form>
//                 <br/>
//                 <div>
//                     Entered Value:{this.state.inputValue}
//                 </div>
//             </div>
//         );
//     }
//
//
// }
//
// ReactDOM.render(<App />,
//     document.querySelector('#root'))

class App extends Component{

}

export default App;
