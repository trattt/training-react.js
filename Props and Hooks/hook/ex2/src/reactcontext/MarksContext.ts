// Sau đây ta sẽ học React Context (tương tự Redux)
// Ta có quan hệ kế thừa như sau: A->B->C
// Thông thường muốn kế thừa ta phải tuần tự từng bước nhưng React Context giúp
// chúng ta truyền trực tiếp từ A->C mà không thông qua B
// CÁI NÀY HỌC SAU DO NÓ LÀ TYPESCRIPT

interface MarksContext{
    name:string;
    marks:number;
    // const contextmarks = React.createContext(null);

}