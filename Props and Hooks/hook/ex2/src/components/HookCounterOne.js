import React, {useEffect, useState} from "react";
import ReactDOM from "react-dom/client";
import {render} from "@testing-library/react";

function HookCounterOne(){
        const [count, setCount] = useState(0);

        useEffect(() => {
            document.title='You clicked '+count+' times'
            // document.title='You clicked '+count+' times'
        })

        return (
            <div>
                <button onClick={()=>setCount(count+1)}>
                    Click {count} times
                </button>
            </div>
        );
    }

export default HookCounterOne