import {useState} from "react"
import {useEffect} from "react";

function Example(){
    const [count,setCount]=useState(0);

    useEffect(() => {
        // Cập nhật tiêu đề trang web sử dụng API trình duyệt
        document.title = `You clicked ${count} times`;
    });


    return (
        <div>
            <p>You clicked {count} time</p>
            <button onClick={()=>setCount(count+1)}>
                Click on
            </button>
        </div>
    );
    }

    function ExampleWithManyStates(){
        const [age, setAge] = useState(42);
        const [fruit, setFruit] = useState('chuối');
        const [todos, setTodos] = useState([{ text: 'Học Hooks' }]);
    }

function FriendStatus(props) {
    const [isOnline, setIsOnline] = useState(null);

    function handleStatusChange(status) {
        setIsOnline(status.isOnline);
    }

    useEffect(() => {
        ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);
        return () => {
            ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
        };
    });

    if (isOnline === null) {
        return 'Loading...';
    }
    return isOnline ? 'Online' : 'Offline';
}

function FriendStatusWithCounter(props) {
    const [count, setCount] = useState(0);
    useEffect(() => {
        document.title = `Bạn đã bấm ${count} lần`;
    });

    const [isOnline, setIsOnline] = useState(null);
    useEffect(() => {
        ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);
        return () => {
            ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
        };
    });

    function handleStatusChange(status) {
        setIsOnline(status.isOnline);
    }
}