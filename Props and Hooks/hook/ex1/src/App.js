import logo from './logo.svg';
import './App.css';
import {useState} from "react";

function App() {
  // return (
  //   <div className="App">
  //     <header className="App-header">
  //       <img src={logo} className="App-logo" alt="logo" />
  //       <p>
  //         Edit <code>src/App.js</code> and save to reload.
  //       </p>
  //       <a
  //         className="App-link"
  //         href="https://reactjs.org"
  //         target="_blank"
  //         rel="noopener noreferrer"
  //       >
  //         Learn React
  //       </a>
  //     </header>
  //   </div>
  // );

  // const name=useState('Trà');
  //
  // const [click,setClick]=useState(0);
  // return(
  // <div>
  //   <p>
  //     {name} has learned React for {click} hours.
  //   </p>
  //   <p>
  //     The number of times you have clicked is {click%2==0?'even!':'old!'}
  //   </p>
  //   <button onClick={()=> setClick(click+1)}>
  //     Study
  //   </button>
  // </div>
  // );

    const [click, setClick] = useState([]);

    const addNumber = () => {
        setClick([
            ...click,
            {
                id: click.length,
                value: Math.random() * 10
            }
        ]);
    };


    return (
        <div>
            <ul>
                {click.map(item => (
                    <li key={item.id}>{item.value}</li>
                ))}
            </ul>
            <button onClick={addNumber}>
                Click me
            </button>
        </div>
    );



}

export default App;
