import PropTypes from "prop-types"
import React from "react";
import ReactDOM from "react-dom/client";
class Greeting extends React.Component{
    // Từ ES2022 bạn có thể khai báo defaultProps là static trong React Component class.
    // static defaultProps={
    //     name: 'stranger'
    // }
    render() {
        return (
            <h1>Hello, {this.props.name}</h1>
        );
    }
}

Greeting.propTypes={
    name: PropTypes.string
};


// Có thể xác định giá trị mặc định cho  bằng cách gán cho defaultProps đặc biệt
// defaultProps sẽ được sử dụng để đảm bảo rằng this.props.name sẽ có giá trị nếu nó không được chỉ định bởi
// parent component. propTypes typechecking sẽ áp dụng cho defaultProps
Greeting.defaultProps={
    name:'Stranger'
}

// Render "Hello, Stranger":
const root=ReactDOM.createRoot(document.getElementById('example'));
root.render(<Greeting/>);


export default Greeting;