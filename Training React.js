- React.JS sử dụng mô hình Single page Application: toàn bộ resource của web bao gồm các file CSS, 
Javascript, master layout hay cấu trúc web page sẽ được load lần đầu tiên khi chúng ta bắt đầu 
duyệt môt website A nào đó

- Java Script:
   +let và const: là những cách tạo biến khác nhau
     Var vẫn hoạt động nhưng khuyến khích dùng let và const
	 .Sử dụng let nếu muốn tạo biến thực sự là biến
	 .Sử dụng const nếu muốn tạo hằng số
	 Ta có thể dùng jsbin.com (trình soạn thảo web) và xem output (tắt HTML và bật Javascript)
	 và console:
	   Dùng var: có thể thay đổi giá trị của biến
	   Dùng const: thay đổi giá trị biến thì chỉ log ra được giá trị biến khi chưa thay đổi, lần 
	   log tiếp theo khi đã thay đổi thì bị lỗi
	   
   +ArrowFuntion: cú pháp để tạo các hàm trong JS
	   Thông thường:
	     function myFnc(){
		 ...
		 }
       Cú pháp ArrowFuntion:
		  const myFnc=()=>{
		  ...
		  }
		  eg:
		     JS:
		     function printMyName(name){
			 console.log(name);
			 }
			 printMyName('Tra');
			 
			 Console:
			 "Tra"
			 
			 JS:
		     const printMyName=(name)=>{
			 console.log(name);
			 }
			 printMyName('Tra');
			 
			 Console:
			 "Tra"
			 
			 Tuy cho kết quả như nhau nhưng cách thứ 2 (const) tạo ra hằng số. Nếu chỉ có 1 đối 
			 số có thể bỏ dấu () trong (name). Nếu không có đối số thì phải truyền ().
			 
			 eg khác:
			    const multiply=(number)=>{
				return number*2;
				}
				console.log(multiply(2))
				
				Conssole:
				4
				
				Rút gọn hàm:
				const multiply=number=>number*2;
				console.log(multiply(2))
				
	Import và export:
		Default export:
		    import peson from'./person.js'	
			import prs from'./person.js'	
		named export:
			import (smth)from './person.js'	
			import (smth as Smth)from './utility.js'	
			import*as bundled from	'./utility.js'	
				
	+Class:	tương tự như Java		
		    class Human{
        constructor(){
            this.gender='female';
        }
        printGender (){
            console.log(this.gender);
        }
    }

    class Person extends Human{
        constructor(){
            super();
            this.name='Tra';
        }

        printMyName(){
            console.log(this.name);
        }
    }
        const person=new Person();
        person.printMyName();
        person.printGender();	
		
	+Classes, Properties, Method	
        class Human{
  gender='female';

printGender=()=>{
            console.log(this.gender);
        }
    }

    class Person extends Human{
      name='Tra';
gender='female';

        printMyName(){
            console.log(this.name);
        }
    }
        const person=new Person();
        person.printMyName();
        person.printGender();
		
	+Spread và Rest:	(....)
		Spread: chia nhỏ mảng thành nhiều Object
		     eg 1:
			 const person={
				 namee:'Tra'
				 }
				 const newPerson={
					 ...person,
					 age: 25
					 }
					 console.log(newPerson);

			 
			 Console:
			 [1, 2, 3, 4]
			 
		     eg 2:
			 const numbers =[1,2,3];
			 const newNumbers=[...numbers,4];
			 console.log(newNumbers);
			 
			 Console:
			 [object Object] {
  age: 25,
  namee: "Tra"
}		 
			 
		Rest: hợp nhất 1 list function thành 1 mảng
		     eg:
			    //Lọc các phần từ có giá trị bằng 1
				const filter=(...args)=>{
  return args.filter(el=>el===1);
}
console.log(filter(1,2,3));
				
				console:
				[1]
				
	+Destructuring: cho phép trích xuất các phần tử mảng hoặc thuộc tính đối tượng  và lưu trữ
	chúng trong các biến; nó khác so với spread
		Sự khác nhau giữa Spread và Destructuring:		
			Spread: lấy tất cả các thuộc tính	
			Destructuring: cho phép lấy ra các phần tử hoặc thuộc tính đơn lẻ và lưu trữ chúng 
			cho mảng và đối tượng	
		eg:		
			const numbers=[1,2,3];
			[num1, ,num3]=numbers;
			console.log(num1,num3)
				
			console:	
			1
			3	
				
	+Reference và Primitive Types	
		Primitive Types (Các kiểu nguyên thủy): số, string, Boolean,...		
		Reference: gán lại cho biến một biến lưu trữ trong 1 biến khác (lưu ý chỉ sao chép con 
		trỏ chứ không sao chép giá trị nên dù cách tạo biến là const thì vẫn có thể thay đổi
		giá trị được)	
				
		eg:		
			// Kiểu Primitive
			const number =1;
			
			// Kiểu reference
			const num2=number;
			
			console.log(num2);	
				
	+Array Functions			
		eg:		
			const numbers=[1,2,3];
			const doubleNumArray=numbers.map((num)=>{
				return num*2
				})
				console.log(numbers);
				console.log(doubleNumArray);	
				
			console:			
			[1, 2, 3]
			[2, 4, 6]	
				
		Có 3 hàm quan trọng được sử dụng với mảng: map(), filter(), reduce() (nhận vào mộ mảng và tính toán với giá trị duy nhất)		
		Trong khóa học có những hàm sau: map, find, findIndex, filter, reduce, concat, slice, plice		
				
				
- React.JS				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
	+end