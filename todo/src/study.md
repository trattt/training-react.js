# HOOK
- Hook: mang Ý nghĩa gắn vào
- Có 2loại component:
  + Class component: đầy đủ tính năng
  + Function component: chỉ dùng để làm ra các UI component chứa những đọan code jsx để gen ra UI
- Hook giúp function component đầy đủ hơn, không thua kém class component và có ưu điểm (đi làm dùng hook thay vì class
  component)
- Hook (bản chất là method, hàm được cung cấp bởi ReactJS)
- Dùng class component khi muốn dùng tính kế thừa, nhìn nó clean hơn, dễ dàng triển khai hơn
- Có thể sử dụng kết hợp function và class component

#### lOẠI 1: useState - Trạng thái của dữ liệu